import fs from 'fs';
import path from 'path';
import marked from 'marked';

const POSTSDIR = 'posts';

export function getPosts () {
    const slugs = fs.readdirSync(POSTSDIR)
        .filter(file => path.extname(file) === '.md')
        .map(file => file.slice(0, -3));
 
    return slugs.map(getPost)
        .filter(post => post.metadata.published !== "false")
        .sort((a, b) => {
            return a.metadata.date < b.metadata.date ? 1 : -1;
        });
}

export function getPost(slug) {
    const file = `${POSTSDIR}/${slug}.md`;

    if (!fs.existsSync(file)) return null;

    const markdown = fs.readFileSync(file, 'utf-8');

    const { content, metadata } = process_markdown(markdown);


    const html = marked(content);

    return {
        slug,
        metadata,
        html
    };
}

function process_markdown(markdown) {
	const match = /---\n([\s\S]+?)\n---/.exec(markdown);
	const frontMatter = match[1];
	const content = markdown.slice(match[0].length);

	const metadata = {};
	frontMatter.split('\n').forEach(pair => {
		const colonIndex = pair.indexOf(':');
		const propertyName = pair.slice(0, colonIndex).trim();
		let propertyValue = pair.slice(colonIndex + 1).trim();
		if (propertyName == 'tags') {
			try {
				propertyValue = JSON.parse(propertyValue.replace(/'/g,'"'));
			} catch (error) {
				propertyValue = [];
			}
		}
		metadata[propertyName] = propertyValue;
	});

	return { metadata, content };
}
