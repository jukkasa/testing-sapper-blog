import { getPosts } from './_posts.js';

let contents;

export function get(req, res) {
	if (!contents || process.env.NODE_ENV !== 'production') {
		const posts = getPosts().map(post => ({
			title: post.metadata.title,
			slug: post.slug,
			cover_image: post.metadata.cover_image,
			description: post.metadata.description,
			date: post.metadata.date,
			tags: post.metadata.tags
		}));

		contents = JSON.stringify(posts);
	}

	res.writeHead(200, {
		'Content-Type': 'application/json'
	});

	res.end(contents);
}