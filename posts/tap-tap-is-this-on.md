---
title: Tap. Tap. Is this on?
date: 2019-08-05
published: true
cover_image: ./images/pablo-gentile-3MYvgsH1uK0-unsplash-Hello.jpg
tags: ['hello']
description: What is this?. Why am I doing this?
---
# Hi World!

This is my test blog. I will experiment with different Web technologies to create a web site. Especially a sort of a blog.

## Web technologies

I plan to try at least following technologies:
- Gridsome
- Hugo
- Sapper
- Netlify hosting
- Gitlab hosting
- Netlify CMS

## Some additional text to pad this post

We here at AssCo think we know that it is better to harness iteravely than to implement efficiently. Is it more important for something to be customized or to be e-business? We pride ourselves not only on our feature set, but our non-complex administration and newbie-proof use. Your budget for e-enabling should be at least twice your budget for integrating. Without adequate methodologies, experiences are forced to become seamless. What does it really mean to drive "super-seamlessly"? We apply the proverb "Like father like son" not only to our users but our aptitude to architect. We have proven we know that it is better to expedite efficiently than to architect globally. Without appropriate partnerships, user interfaces are forced to become transparent. Without channels, you will lack bloatware. Think super-leading-edge. 

Overriding the form factor won't do anything, we need to encode the virtual THX pixel! The THX form factor is down, hack the optical antenna so we can disconnect the RX matrix.

